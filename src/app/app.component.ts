import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angularBasics1679573';

  alumnosDummy: string[] = ['barbara', 'armando'];

  onAddAlumno(name: string) {
    this.alumnosDummy.push(name);
  }
}
